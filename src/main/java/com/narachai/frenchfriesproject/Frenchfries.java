/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.frenchfriesproject;

/**
 *
 * @author ASUS
 */
public class Frenchfries {
    private int S;
    private int M;
    private int L;
    private int X;
    private int number;
    private char defaultsize = 'S';
    public boolean FF(char size){
        switch(size){
            case 'S':
                S+=1;
                break;
            case 'M':
                M+=1;
                break;
            case 'L':
                L+=1;
                break;
            case 'X':
                X+=1;
                break;
        }
       return true; 
    }
    
    public boolean FF(char size , int number){
        for(int i=0;i<number;i++){
            if(!this.FF(size)){
                return false;
            }
        }
        return true;
    }
    
    public boolean FF(){
        return this.FF(defaultsize);
    }
    
    public boolean FF(int number){
        return this.FF(defaultsize, number);
    }
    
    @Override
    public String toString() {
        return "Frenchfries"+"\nsize S = "+S+"\nsize M = "+M+"\nsize L = "+L+"\nsize X = "+X+"\n*****************************";
    }

    
}
